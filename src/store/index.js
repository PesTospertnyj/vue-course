import {createStore} from 'vuex'
import auth from '@/store/modules/auth'

const store = createStore({
  state() {},
  mutations: {},
  actions: {},
  modules: {auth},
})

export default store
